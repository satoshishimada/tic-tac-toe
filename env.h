
#ifndef __ENV_H__
#define __ENV_H__

#include <vector>

enum BOARD_STATUS {
	NONE,
	OWNER,
	COMPUTER,
};

struct observation
{
	std::vector<int> board;
};

struct result
{
	struct observation obs;;
	double reward;
	bool   done;
};

struct action
{
	int position;
};

class Env
{
public:
	virtual struct observation reset(void) = 0;
	virtual struct result step(struct action) = 0;
	virtual std::vector<struct action> getActions(void) = 0;
	virtual void viewStatus(void) = 0;
};

#endif // __ENV_H__

