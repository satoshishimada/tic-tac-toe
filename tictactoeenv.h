
#include "env.h"

class TicTacToeEnv : public Env
{
public:
	TicTacToeEnv(int);
	~TicTacToeEnv();
	struct observation reset(void);
	struct result step(struct action);
	std::vector<struct action> getActions(void);
	void viewStatus(void);
private:
	void reset_board(void);
	void set_board(int);
	void actionOwner(void);
	struct action randomAction(std::vector<struct action>);
	double reward(void);
	bool isDone(void);
	const int board_num;
	const int field_size;
	std::vector<int> board;
	int win_side;
};

