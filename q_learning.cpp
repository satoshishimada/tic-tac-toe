
#include "tictactoeenv.h"
#include "agent.h"
#include <iostream>
#include <sstream>
#include <string>

int main(int argc, char *argv[])
{
	int max_loop = 1000000; /* default iteration: 1e6 */
	if(argc == 2) {
		std::stringstream ss;
		ss << argv[1] << std::flush;
		ss >> max_loop;
	}
	TicTacToeEnv env(3);
	Agent agent(19683, 9);
	double ave_reward = 0;
	for(int i = 0; i < max_loop; i++) {
		struct result ret;
		ret.obs = env.reset();
		for(;;) {
			std::vector<struct action> actions;
			actions = env.getActions();
			struct action action;
			action = agent.getAction(ret.obs, actions);
			ret = env.step(action);
			actions = env.getActions();
			agent.update(ret.obs, ret.reward, actions);
			ave_reward += ret.reward;
			if(ret.done) {
				break;
			}
		}
		if((i + 1) % 1000 == 0) {
			std::cout << (ave_reward / 1000.) << std::endl;
			ave_reward = 0;
		}
	}
	agent.saveParameters(std::string("q-learning.param"));

	return 0;
}

