
#include "tictactoeenv.h"
#include <iostream>
#include <sstream>
#include <random>

int main(int argc, char *argv[])
{
	int max_loop = 1000000; /* default iteration: 1e6 */
	if(argc == 2) {
		std::stringstream ss;
		ss << argv[1] << std::flush;
		ss >> max_loop;
	}
	std::random_device rnd;
	std::mt19937 mt(rnd());
	TicTacToeEnv env(3);
	std::vector<struct action> actions;
	double ave_reward = 0;
	for(int i = 0; i < max_loop; i++) {
		struct result ret;
		ret.obs = env.reset();
		for(;;) {
			actions = env.getActions();
			std::uniform_int_distribution<> rint(0, actions.size() - 1);
			struct action action = actions[rint(mt)];
			ret = env.step(action);
			ave_reward += ret.reward;
			if(ret.done) {
				break;
			}
		}
		if((i + 1) % 1000 == 0) {
			std::cout << (ave_reward / 1000.) << std::endl;
			ave_reward = 0;
		}
	}

	return 0;
}

