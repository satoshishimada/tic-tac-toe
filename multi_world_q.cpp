
#include "tictactoeenv.h"
#include "agent.h"
#include <iostream>
#include <sstream>
#include <string>
#include <cmath>

int main(int argc, char *argv[])
{
	int max_loop = 1000000; /* default iteration: 1e6 */
	if(argc == 2) {
		std::stringstream ss;
		ss << argv[1] << std::flush;
		ss >> max_loop;
	}
	const int update_step = 1000;
	const int print_step = 1000;
	const int world_num = 5;
	double epsilon = 0.3;
	std::vector<TicTacToeEnv> env;
	std::vector<Agent> agent;
	std::vector<double> ave_reward;
	std::vector<double> evaluation_value;
	for(int i = 0; i < world_num; i++) {
		env.push_back(TicTacToeEnv(3));
		agent.push_back(Agent(19683, 9));
		agent[i].setEpsilon(epsilon);
		ave_reward.push_back(0);
		evaluation_value.push_back(0);
	}
	const double dfe = 0.5; /* discount factor for evaluation value */
	for(int iter = 0; iter < max_loop; iter++) {
		std::vector<std::vector<double>> reward_history(world_num);
		std::vector<struct result> ret;
		for(int i = 0; i < world_num; i++) {
			//evaluation_value[i] = 0;
			struct result r;
			r.obs = env[i].reset();
			ret.push_back(r);
		}
		for(int i = 0; i < world_num; i++) {
			for(;;) {
				std::vector<struct action> actions;
				actions = env[i].getActions();
				struct action action;
				action = agent[i].getAction(ret[i].obs, actions);
				ret[i] = env[i].step(action);
				actions = env[i].getActions();
				agent[i].update(ret[i].obs, ret[i].reward, actions);
				reward_history[i].push_back(ret[i].reward);
				ave_reward[i] += ret[i].reward;
				if(ret[i].done) {
					break;
				}
			}
			if((iter + 1) % print_step == 0) {
				std::cout << (ave_reward[i] / print_step) << " ";
				ave_reward[i] = 0;
			}
		}
		if((iter + 1) % print_step == 0) {
			std::cout << std::endl;
		}
		/* calc evaluation value for each world */
		for(int i = 0; i < world_num; i++) {
			int episode_num = reward_history[i].size();
			for(int j = 0; j < episode_num; j++) {
				evaluation_value[i] += pow(dfe, episode_num - (j + 1)) * reward_history[i][j];
			}
			evaluation_value[i] /= update_step;
		}
		if((iter + 1) % update_step == 0) {
			int best_agent_index = 0;
			double best_eva = evaluation_value[0];
			for(int i = 0; i < world_num; i++) {
				if(best_eva < evaluation_value[i]) {
					best_agent_index = i;
					best_eva = evaluation_value[i];
				}
				evaluation_value[i] = 0;
			}
			epsilon = exp(-iter/100000.);
			for(int i = 0; i < world_num; i++) {
				agent[i].setEpsilon(epsilon);
				if(i != best_agent_index) {
					agent[i].setParam(agent[best_agent_index].getParam());
				}
			}
		}
	}
	agent[0].saveParameters(std::string("q-learning.param"));

	return 0;
}

