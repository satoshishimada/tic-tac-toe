
#include <fstream>
#include <random>
#include "agent.h"

Agent::Agent(int sta_dim, int act_dim)
{
	action_dim = act_dim;
	state_dim = sta_dim;
	lr = 0.01;
	df = 0.8;
	epsilon = 0.05;
	q_table = std::vector<double>(state_dim * action_dim);
	resetQTable();
}

Agent::~Agent()
{
}

struct action Agent::getAction(struct observation obs, std::vector<struct action> actions)
{
	std::random_device rnd;
	std::mt19937 mt(rnd());
	std::uniform_real_distribution<> rand1(0, 1.0);
	double e = rand1(mt);
	struct action act = actions[0];
	double max_q = q_table[0];
	if(e > epsilon) {
		for(int i = 0; i < actions.size(); i++) {
			if(max_q < q_table[getQIndex(obs, actions[i])]) {
				act = actions[i];
				max_q = q_table[getQIndex(obs, act)];
			}
		}
	} else {
		std::uniform_int_distribution<> randint(0, actions.size() - 1);
		act = actions[randint(mt)];
		max_q = q_table[getQIndex(obs, act)];
	}
	old_act = act;
	old_obs = obs;
	return act;
}

void Agent::update(struct observation obs, double reward, std::vector<struct action> actions)
{
	double max_q = q_table[0];
	for(int i = 0; i < actions.size(); i++) {
		if(max_q < q_table[getQIndex(obs, actions[i])]) {
			max_q = q_table[getQIndex(obs, actions[i])];
		}
	}
	q_table[getQIndex(old_obs, old_act)] += lr * (reward + df * max_q - q_table[getQIndex(old_obs, old_act)]);
}

int Agent::getQIndex(struct observation observation, struct action act)
{
	int ratio = 1;
	int index = 0;
	for(int i = 0; i < observation.board.size(); i++) {
		index += observation.board[i] * ratio;
		ratio *= 3;
	}
	return index * act.position;
}

void Agent::resetQTable(void)
{
	std::random_device rnd;
	std::mt19937 mt(rnd());
	std::normal_distribution<> normal(0.0, 1.0); /* average: 0.0, variance: 1.0 */
	for(int i = 0; i < q_table.size(); i++) {
		q_table[i] = normal(mt);
	}
}

void Agent::saveParameters(std::string filename)
{
	std::ofstream outfile;
	outfile.open(filename, std::ios::out);
	if(!outfile) {
		throw std::string("error");
		return;
	}
	for(int i = 0; i < q_table.size(); i++) {
		outfile << q_table[i] << std::endl;
	}
	outfile.close();
}

void Agent::loadParameters(std::string filename)
{
	std::ifstream infile;
	infile.open(filename);
	if(!infile) {
		throw std::string("error");
		return;
	}
	for(int i = 0; i < q_table.size(); i++) {
		infile >> q_table[i];
	}
}

std::vector<double> Agent::getParam(void)
{
	std::vector<double> ret(q_table.size());
	for(int i = 0; i < q_table.size(); i++) {
		ret[i] = q_table[i];
	}
	return ret;
}

void Agent::setParam(std::vector<double> param)
{
	for(int i = 0; i < q_table.size() && i < param.size(); i++) {
		//q_table[i] = param[i];
		q_table[i] = (param[i] + q_table[i]) / 2.0;
	}
}

void Agent::setEpsilon(double new_epsilon)
{
	epsilon = new_epsilon;
}

