
CXX=g++
CFLAGS=-Wall -std=c++11
LIBS=-lm

OBJS=tictactoeenv.o agent.o

all: q-learning random-agent multi-world

q-learning: $(OBJS) q_learning.o
	$(CXX) $(CFLAGS) -o $@ $^ $(LIBS)

random-agent: $(OBJS) random_agent.o
	$(CXX) $(CFLAGS) -o $@ $^ $(LIBS)

multi-world: $(OBJS) multi_world_q.o
	$(CXX) $(CFLAGS) -o $@ $^ $(LIBS)

%.o: %.cpp
	$(CXX) -c $(CFLAGS) $^

.PHONY: clean
clean:
	rm -f $(OBJS)

