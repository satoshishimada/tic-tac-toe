
#ifndef __AGENT_H__
#define __AGENT_H__

#include <string>
#include "env.h"

class Agent
{
public:
	Agent(int, int);
	~Agent();
	struct action getAction(struct observation, std::vector<struct action>);
	void update(struct observation, double, std::vector<struct action>);
	void saveParameters(std::string);
	void loadParameters(std::string);
	std::vector<double> getParam(void);
	void setParam(std::vector<double>);
	void setEpsilon(double);
protected:
	std::vector<double> q_table;
	int getQIndex(struct observation, struct action);
	void resetQTable(void);
	int state_dim;
	int action_dim;
	struct action old_act;
	struct observation old_obs;
	double lr; /* learning rate */
	double df; /* discount factor */
	double epsilon;
};

#endif // __AGENT_H__

