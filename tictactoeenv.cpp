
#include "tictactoeenv.h"
#include <iostream>
#include <random>

TicTacToeEnv::TicTacToeEnv(int num) : board_num(num), field_size(num * num), board(num * num)
{
	win_side = NONE;
}

TicTacToeEnv::~TicTacToeEnv()
{
}

struct observation TicTacToeEnv::reset(void)
{
	reset_board();
	struct observation obs;
	obs.board = this->board;
	return obs;
}

struct result TicTacToeEnv::step(struct action action)
{
	struct result res;
	set_board(action.position);
	if(isDone()) {
		res.obs.board = this->board;
		res.reward = reward();
		res.done = true;
		return res;
	}
	actionOwner();
	if(isDone()) {
		res.obs.board = this->board;
		res.reward = reward();
		res.done = true;
		return res;
	}
	res.obs.board = this->board;
	res.reward = reward();
	res.done = isDone();
	return res;
}

void TicTacToeEnv::reset_board(void)
{
	board.resize(field_size);
	for(int i = 0; i < field_size; i++) {
		board[i] = NONE;
	}
}

void TicTacToeEnv::set_board(int position)
{
	board[position] = COMPUTER;
}

void TicTacToeEnv::actionOwner(void)
{
	std::vector<struct action> actions;
	actions = getActions();
	struct action act = randomAction(actions);
	board[act.position] = OWNER;
}

struct action TicTacToeEnv::randomAction(std::vector<struct action> actions)
{
	struct action act;
	std::random_device rnd;
	std::mt19937 mt(rnd());
	std::uniform_int_distribution<> random_index(0, actions.size()-1);
	act = actions[random_index(mt)];
	return act;
}

std::vector<struct action> TicTacToeEnv::getActions(void)
{
	std::vector<struct action> actions;
	struct action act;
	for(int i = 0; i < field_size; i++) {
		if(board[i] == NONE) {
			act.position = i;
			actions.push_back(act);
		}
	}
	return actions;
}

double TicTacToeEnv::reward(void)
{
	if(isDone()) {
		if(win_side == OWNER)
			return -100;
		else
			return 100;
	}
	return 0;
}

bool TicTacToeEnv::isDone(void)
{
	win_side = NONE;
	bool fill_field = true;
	for(int i = 0; i < field_size; i++) {
		if(board[i] == NONE) {
			fill_field = false;
			break;
		}
	}
	if(fill_field) return true;
	for(int y = 0; y < board_num; y++) {
		int side = board[y * board_num + 0];
		if(side == NONE) continue;
		for(int x = 1; x < board_num; x++) {
			if(board[y * board_num + x] != side) {
				goto next_row_loop;
			}
		}
		win_side = side;
		return true;
next_row_loop: ;
	}
	for(int x = 0; x < board_num; x++) {
		int side = board[0 * board_num + x];
		if(side == NONE) continue;
		for(int y = 1; y < board_num; y++) {
			if(board[y * board_num + x] != side) {
				goto next_col_loop;
			}
		}
		win_side = side;
		return true;
next_col_loop: ;
	}
	int side = board[0 * board_num + 0];
	if(side == NONE) goto next_check;
	for(int y = 1, x = 1; y < board_num; y++, x++) {
		if(board[y * board_num + x] != side) {
			goto next_check;
		}
	}
	win_side = side;
	return true;
next_check:
	side = board[0 * board_num + (board_num - 1)];
	if(side == NONE) return false;
	for(int y = 1, x = (board_num - 1); y < board_num; y++, x--) {
		if(board[y * board_num + x] != side) {
			return false;
		}
	}
	win_side = side;
	return true;
}

void TicTacToeEnv::viewStatus(void)
{
	std::cout << "+";
	for(int y = 0; y < board_num; y++)
		std::cout << "-";
	std::cout << "+" << std::endl;
	for(int y = 0; y < board_num; y++) {
		std::cout << "|";
		for(int x = 0; x < board_num; x++) {
			int s = board[y * board_num + x];
			if(s == NONE) {
				std::cout << " ";
			} else if(s == OWNER) {
				std::cout << "X";
			} else {
				std::cout << "O";
			}
		}
		std::cout << "|" << std::endl;
	}
	std::cout << "+";
	for(int y = 0; y < board_num; y++)
		std::cout << "-";
	std::cout << "+" << std::endl;
}

